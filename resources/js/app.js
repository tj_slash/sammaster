
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import Vue2TouchEvents from 'vue2-touch-events';
Vue.use(Vue2TouchEvents, {swipeTolerance: 100});

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('request', require('./components/RequestComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    methods: {
        setProductRequest: function(product_id) {
        	this.$refs.request.product_id = product_id;
        	this.$refs.request2.product_id = product_id;
        }
    }
});

ymaps.ready(init);
function init() {
    var map = new ymaps.Map("map", {
        center: [50.295365, 127.552479],
        zoom: 13,
        controls: ['zoomControl']
    }),
    placemark = new ymaps.Placemark(map.getCenter());
    map.geoObjects.add(placemark);
}