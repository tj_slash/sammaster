@extends('layouts.index')

@section('title', Config::get('settings.meta_title'))
@section('keywords', Config::get('settings.meta_keywords'))
@section('description', Config::get('settings.meta_description'))

@section('content')
	<div class="main-background position-relative padding-1 flex-container align-center align-middle vh100">
		<div class="grid-container text-center position-relative main-background__wrapper">
			<img src="{{ asset('images/master_big.png') }}" class="margin-bottom-1" alt="Наш инструмент вам в помощь" title="Наш инструмент вам в помощь" />
			<hgroup>
				<h2 class="text-uppercase font-white">
					<strong>Наш инструмент вам</strong>
				</h2>
				<h3 class="text-uppercase font-green">
					<strong>в помощь</strong>
				</h3>
				<a href="{{ route('index') }}#request" class="button radius button-border-top success text-uppercase font-white font22">
					Оставить заявку
				</a>
			</hgroup>
		</div>
	</div>
	<div id="why" class="padding-vertical-3 flex-container align-center align-middle vh100">
		<div class="grid-container text-center position-relative width-100">
			<div class="margin-bottom-1">
				<strong class="font36">Преимущества нашего магазина</strong>
			</div>
			<div class="margin-bottom-1 font20 font-medium-gray">
				Почему выбирают наш магазин
			</div>
			<div class="margin-bottom-1">
				<img src="{{ asset('images/master_small.png') }}" alt="Преимущества нашего магазина" title="Преимущества нашего магазина" />
			</div>
			<div class="margin-bottom-1">
				<div class="grid-x grid-margin-x show-for-small-only">
					<div class="cell small-4 text-center flex-container align-center align-middle">
						<img src="{{ asset('images/check.png') }}" />
					</div>
					<div class="cell small-4 text-center flex-container align-center align-middle">
						<img src="{{ asset('images/tag.png') }}" />
					</div>
					<div class="cell small-4 text-center flex-container align-center align-middle">
						<img src="{{ asset('images/car.png') }}" />
					</div>
					<div class="cell small-4 text-center">
						<strong class="display-block font20">Скидки и акции</strong>
					</div>
					<div class="cell small-4 text-center">
						<strong class="display-block font20">Доступные цены</strong>
					</div>
					<div class="cell small-4 text-center">
						<strong class="display-block font20">Доставка по России</strong>
					</div>
				</div>
				<div class="grid-x grid-margin-x hide-for-small-only margin-horizontal-3 padding-horizontal-3 margin-bottom-3">
					<div class="cell small-4 text-center flex-container align-center align-middle">
						<img src="{{ asset('images/check.png') }}" />
					</div>
					<div class="cell small-4 text-center flex-container align-center align-middle">
						<img src="{{ asset('images/tag.png') }}" />
					</div>
					<div class="cell small-4 text-center flex-container align-center align-middle">
						<img src="{{ asset('images/car.png') }}" />
					</div>
					<div class="cell small-4 text-center">
						<strong class="display-block font20">Скидки и акции</strong>
					</div>
					<div class="cell small-4 text-center">
						<strong class="display-block font20">Доступные цены</strong>
					</div>
					<div class="cell small-4 text-center">
						<strong class="display-block font20">Доставка по России</strong>
					</div>
				</div>
			</div>
			<div class="font19 font-medium-gray margin-bottom-1">
				Наша цель - отличный сервис и выгодные цены.
			</div>
		</div>
	</div>
	<request inline-template ref="request">
		<div id="request" class="padding-vertical-3 flex-container align-center align-middle background-dark">
			<div class="grid-container text-center width-100">
				<div class="margin-bottom-1">
					<strong class="font36 text-uppercase font-green">Заполните форму заявки</strong>
				</div>
				<div class="margin-bottom-2 font25 font-medium-gray">
					Оставьте заявку на сайте, и мы с Вами свяжемся
				</div>
				<div class="margin-bottom-2">
					<div class="grid-x grid-margin-x">
						<div class="cell small-12 medium-3">
							<input required type="text" placeholder="* имя" v-model="name" autocomplete="off" />
						</div>
						<div class="cell small-12 medium-3">
							<input required type="tel" placeholder="* телефон" v-model="phone" autocomplete="off" />
						</div>
						<div class="cell small-12 medium-3">
							<input type="email" placeholder="Email" v-model="email" autocomplete="off" />
						</div>
						<div class="cell small-12 medium-3">
							<input required type="text" placeholder="* город/индекс" v-model="city" autocomplete="off" />
						</div>
					</div>
				</div>
				<div class="font16 font-medium-gray margin-bottom-1">
					Нажимая на кнопку, Вы принимаете <a href="{{ route('page', ['polozhenie']) }}" class="font-green" target="_blank">Положение</a> и <a href="{{ route('page', ['soglasie']) }}" class="font-green" target="_blank">Согласие</a> на обработку персональных данных
				</div>
				<div class="font19 font-medium-gray margin-bottom-1">
					<a @click="submit" class="button radius button-border-top success text-uppercase font-white font22">
						Подтвердить
					</a>
				</div>
				<div class="font19 font-medium-gray margin-bottom-1">
					Для того чтобы оставить заявку необходимо  заполнить обязательные поля : имя, телефон, город и нажать кнопку подтвердить.
				</div>
			</div>
		</div>
	</request>
	@if ($products->count())
		<div id="products" class="padding-vertical-3 flex-container align-center align-middle">
			<div class="grid-container">
				<div class="grid-x grid-margin-x products">
					<div class="cell small-12 text-center font36 margin-bottom-3">Товары</div>
					@each('include.product', $products, 'product')
					<div class="cell small-12 text-center margin-bottom-1 hide-for-small-only">
						<a href="{{ route('index') }}#request" class="button radius button-border-top success text-uppercase font-white font31">
							Оставить заявку
						</a>
					</div>
				</div>
			</div>
		</div>
	@endif
	@if ($page_delivery)
		<div id="delivery" class="padding-vertical-3 flex-container align-center align-middle background-medium">
			<div class="grid-container text-center">
				<img src="{{ asset('images/delivery.png') }}" class="margin-bottom-1" alt="{{ $page_delivery->title }}" title="{{ $page_delivery->title }}">
				<div class="margin-bottom-1">
					<strong class="font36 text-uppercase">{{ $page_delivery->title }}</strong>
				</div>
				<div class="margin-bottom-1 font19 text-left">
					{!! $page_delivery->content !!}
				</div>
			</div>
		</div>
	@endif
	<request inline-template ref="request2">
		<div id="request" class="padding-vertical-3 flex-container align-center align-middle background-dark">
			<div class="grid-container text-center">
				<div class="margin-bottom-1">
					<strong class="font36 text-uppercase font-green">Остались вопросы?</strong>
				</div>
				<div class="margin-bottom-2 font25 font-medium-gray">
					Оставьте заявку на сайте прямо сейчас
				</div>
				<div class="margin-bottom-2">
					<div class="grid-x grid-margin-x">
						<div class="cell small-12 medium-3">
							<input required type="text" placeholder="* имя" v-model="name" autocomplete="off" />
						</div>
						<div class="cell small-12 medium-3">
							<input required type="tel" placeholder="* телефон" v-model="phone" autocomplete="off" />
						</div>
						<div class="cell small-12 medium-3">
							<input type="email" placeholder="Email" v-model="email" autocomplete="off" />
						</div>
						<div class="cell small-12 medium-3">
							<input required type="text" placeholder="* город/индекс" v-model="city" autocomplete="off" />
						</div>
					</div>
				</div>
				<div class="font16 font-medium-gray margin-bottom-1">
					Нажимая на кнопку, Вы принимаете <a href="{{ route('page', ['polozhenie']) }}" class="font-green" target="_blank">Положение</a> и <a href="{{ route('page', ['soglasie']) }}" class="font-green" target="_blank">Согласие</a> на обработку персональных данных
				</div>
				<div class="font19 font-medium-gray margin-bottom-1">
					<a @click="submit" class="button radius button-border-top success text-uppercase font-white font22">
						Подтвердить
					</a>
				</div>
				<div class="font19 font-medium-gray margin-bottom-1">
					Для того чтобы оставить заявку необходимо  заполнить обязательные поля : имя, телефон, город и нажать кнопку подтвердить.
				</div>
			</div>
		</div>
	</request>
	<div id="how" class="padding-vertical-3">
		<div class="grid-container text-center">
			<div>
				<strong class="font36">Как сделать заказ</strong>
			</div>
			<div class="margin-bottom-1 font20">
				Пошаговая инструкция
			</div>
			<div class="grid-x grid-margin-x">
				<div class="cell small-12 medium-4 margin-bottom-1 text-center">
					<div class="margin-bottom-1">
						<strong class="font36">Шаг 1</strong>
					</div>
					<div class="margin-bottom-1 position-relative">
						<img src="{{ asset('images/step-1.png') }}" alt="Вы заполняете заявку заполнив форму обратной связи" title="Вы заполняете заявку заполнив форму обратной связи" style="height:300px">
						<a href="{{ route('index') }}#request" class="button radius success text-uppercase font-white font19 position-absolute" style="width:220px;right:15px;top:33%;border:2px solid">
							Оставить заявку
						</a>
					</div>
					<div>
						<strong class="font20">Вы заполняете заявку</strong>
					</div>
					<div class="margin-bottom-1 font19">
						заполнив форму обратной связи
					</div>
				</div>
				<div class="cell small-12 medium-4 margin-bottom-1 text-center">
					<div class="margin-bottom-1">
						<strong class="font36">Шаг 2</strong>
					</div>
					<div class="margin-bottom-1">
						<img src="{{ asset('images/step-2.png') }}" alt="Менеджер перезвонит вам для уточнения деталей заказа" title="Менеджер перезвонит вам для уточнения деталей заказа" style="height:300px">
					</div>
					<div>
						<strong class="font20">Менеджер перезвонит вам</strong>
					</div>
					<div class="margin-bottom-1 font19">
						для уточнения деталей заказа
					</div>
				</div>
				<div class="cell small-12 medium-4 margin-bottom-1 text-center">
					<div class="margin-bottom-1">
						<strong class="font36">Шаг 3</strong>
					</div>
					<div class="margin-bottom-1">
						<img src="{{ asset('images/step-3.png') }}" alt="Отправляем ваш заказ курьером или почтой" title="Отправляем ваш заказ курьером или почтой" style="height:300px">
					</div>
					<div>
						<strong class="font20">Отправляем ваш заказ</strong>
					</div>
					<div class="margin-bottom-1 font19">
						курьером или почтой
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('include.contacts')
@endsection