<div id="contacts" class="background-medium">
	<div class="grid-container">
		<div class="grid-x grid-margin-x">
			<div class="cell small-12 medium-6">
				<div id="map" style="height:400px"></div>
			</div>
			<div class="cell small-12 medium-6 flex-container align-middle padding-vertical-1">
				<div>
					<div class="margin-bottom-1">
						<strong class="font36">Остались вопросы?</strong>
					</div>
					<div class="margin-bottom-1 font19">
						<p><strong>Контактная информация:</strong></p>
						<p>Адрес: {{ Config::get('settings.contact_address') }}</p>
						<p>E-mail: <a href="mailto:{{ Config::get('settings.contact_email') }}" class="font-black">{{ Config::get('settings.contact_email') }}</a></p>
						<p>Телефон: <a href="tel:{{ str_replace(' ', '', Config::get('settings.contact_email')) }}" class="font-black">{{ Config::get('settings.contact_phone') }}</a></p>
					</div>
					<div class="margin-bottom-1">
						<strong class="font19">Мы в соцсетях:</strong>
						<div>
							<a href="{{ Config::get('settings.contact_facebook') }}" target="_blank">
								<img src="{{ asset('images/facebook.png') }}" />
							</a>
							<a href="{{ Config::get('settings.contact_instagram') }}" target="_blank">
								<img src="{{ asset('images/instagram.png') }}" />
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>