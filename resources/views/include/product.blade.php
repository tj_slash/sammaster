<div class="cell small-12 medium-3 products__item margin-bottom-2">
	<div class="flex-container align-middle align-center products__item__wrapperimage">
		@if (!empty($product->images))
			<img src="{{ asset(current($product->images)) }}" alt="{{ $product->title }}" title="{{ $product->title }}" class="products__item__image width-100 margin-bottom-1">
		@else
			<img src="{{ asset('images/not-found.png') }}" alt="{{ $product->title }}" title="{{ $product->title }}" class="products__item__image width-100 margin-bottom-1">
		@endif
	</div>
	<div class="products__item__title margin-bottom-1 font18 font-green text-center">
		{{ $product->title }}
	</div>
	<div class="products__item__description margin-bottom-1 font12 font-medium-gray">
		<div class="products__item__description__wrapper padding-1 text-justify">
			{!! $product->content !!}
			<div class="products__item__description__wrapper_hide margin-top-1">
				<div class="margin-bottom-1 flex-container align-middle align-spaced">
					@if ($product->old_price)
						<div class="font-medium-gray font14 products__item__old-price">
							{{ intval($product->old_price) }} руб.
						</div>
					@endif
					<div class="font-green font18 products__item__price">
						{{ intval($product->price) }} руб.
					</div>
				</div>
				<div>
					<a href="{{ route('index') }}#request" class="button radius success text-uppercase font-white font18 width-100" @click="setProductRequest({{ $product->id }})">
						Оставить заявку
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="margin-bottom-1 flex-container align-middle align-spaced products__item__description__wrapper_show">
		@if ($product->old_price)
			<div class="font-medium-gray font14 products__item__old-price">
				{{ intval($product->old_price) }} руб.
			</div>
		@endif
		<div class="font-green font18 products__item__price">
			{{ intval($product->price) }} руб.
		</div>
	</div>
	<div class="margin-bottom-1 products__item__description__wrapper_show">
		<a class="button radius success text-uppercase font-white font18 width-100">
			Оставить заявку
		</a>
	</div>
</div>