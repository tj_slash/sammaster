<header class="header">
	<div class="header__top">
		<div class="grid-container">
			<div class="grid-x grid-margin-x">
				<div class="cell small-12 text-center medium-text-left">
					<a href="tel:{{ str_replace(' ', '', Config::get('settings.contact_phone')) }}" class="font-green">
						<span class="mdi mdi-phone"></span> {{ Config::get('settings.contact_phone') }}
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="header__bottom padding-vertical-1">
		<div class="grid-container">
			<div class="grid-x grid-margin-x">
				<div class="cell small-12 medium-6 text-center medium-text-left">
					<a href="{{ URL::to('/') }}" title="{{ Config::get('settings.meta_title') }}">
						<img src="{{ asset('images/logo.png') }}" alt="{{ Config::get('settings.meta_title') }}" title="{{ Config::get('settings.meta_title') }}" />
					</a>
				</div>
				<div class="cell small-12 medium-6 text-left hide-for-small-only flex-container align-middle">
					<nav>
						<ul class="menu">
							<li>
								<a href="{{ route('index') }}#why" title="Почему мы" class="font15 font-white">
									Почему мы
								</a>
							</li>
							<li>
								<a href="{{ route('index') }}#products" title="Товары" class="font15 font-white">
									Товары
								</a>
							</li>
							<li>
								<a href="{{ route('index') }}#delivery" title="Доставка" class="font15 font-white">
									Доставка
								</a>
							</li>
							<li>
								<a href="{{ route('index') }}#how" title="Как сделать заказ" class="font15 font-white">
									Как сделать заказ
								</a>
							</li>
							<li>
								<a href="{{ route('index') }}#contacts" title="Контакты" class="font15 font-white">
									Контакты
								</a>
							</li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</div>
</header>