<footer class="footer padding-vertical-1">
	<div class="grid-container">
		<div class="grid-x grid-margin-x">
			<div class="cell small-12 medium-3 margin-bottom-1 flex-container align-middle align-center text-center medium-text-left">
				<img src="{{ asset('images/logo.png') }}" alt="{{ Config::get('settings.meta_title') }}" title="{{ Config::get('settings.meta_title') }}" />
			</div>
			<div class="cell small-12 medium-6 margin-bottom-1">
				<p class="font14 font-white margin-bottom-0">
					{{ Config::get('settings.footer_info') }}<br/>
					Вся информация на сайте - собственность интернет-магазина.<br/>
					Публикация информации с сайта без ссылки на сайт запрещена.
				</p>
			</div>
			<div class="cell small-12 medium-3 margin-bottom-1 flex-container align-middle align-center text-center medium-text-left ">
				<p class="font14 font-green margin-bottom-0">
					&copy; 2019 - {{ date('Y') > 2019 ? date('Y') : '' }} <a href="{{ URL::to('/') }}" title="{{ Config::get('settings.meta_title') }}" class="font-green">{{ URL::to('/') }}</a>
				</p>
			</div>
		</div>
	</div>
</footer>