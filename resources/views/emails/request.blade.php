<h1>Новая заявка на сайте:</h1>
@if ($requestUser->name)
	<p><strong>Имя:</strong> {{ $requestUser->name }}</p>
@endif
@if ($requestUser->phone)
	<p><strong>Телефон:</strong> {{ $requestUser->phone }}</p>
@endif
@if ($requestUser->email)
	<p><strong>E-mail:</strong> {{ $requestUser->email }}</p>
@endif
@if ($requestUser->city)
	<p><strong>Город/индекс:</strong> {{ $requestUser->city }}</p>
@endif
@if ($requestUser->product)
	<p><strong>Товар:</strong> {{ $requestUser->product->title }}</p>
@endif