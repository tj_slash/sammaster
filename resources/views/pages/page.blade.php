@extends('layouts.index')

@section('title', $page->meta_title ? $page->meta_title : $page->title . ' / ' . Config::get('settings.meta_title'))
@section('keywords', $page->meta_keywords ? $page->meta_title : $page->title . ' / ' . Config::get('settings.meta_title'))
@section('description', $page->meta_description ? $page->meta_title : $page->title . ' / ' . Config::get('settings.meta_title'))

@section('content')
	<article class="padding-vertical-1">
		<div class="grid-container">
			<div class="grid-x grid-margin-x">
				<div class="cell small-12 margin-bottom-1">
					<h1 class="font36 font-green">
						<strong>{{ $page->title }}</strong>
					</h1>
				</div>
				<div class="cell small-12 margin-bottom-1">
					{!! $page->content !!}
				</div>
			</div>
		</div>
	</article>
	@include('include.contacts')
@endsection