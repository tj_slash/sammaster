<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'                => 'Значение не принято.',
    'active_url'              => 'Адрес неверен.',
    'after'                   => 'Дата должна быть после :date.',
    'alpha'                   => 'Неверный формат.',
    'alpha_dash'              => 'Неверный формат.',
    'alpha_num'               => 'Неверный формат.',
    'array'                   => 'Должно быть массивом.',
    'before'                  => 'Дата должна быть до :date.',
    'between'                 => [
        'numeric' => 'Должно быть между :min и :max.',
        'file'    => 'Размер файла должен быть от :min до :max килобайт.',
        'string'  => 'Длина должна быть от :min до :max символов.',
        'array'   => 'Должно быть от :min до :max элементов.',
    ],
    'confirmed'               => 'Подтверждение этого поля не совпадает.',
    'date'                    => 'Неверная дата.',
    'date_format'             => 'Неверный формат.',
    'different'               => ':attribute и :other должны различаться.',
    'digits'                  => ':attribute должно быть :digits числом.',
    'digits_between'          => ':attribute должно быть между :min и :max.',
    'email'                   => ':attribute должен быть действующим e-mail.',
    'exists'                  => 'Выбранное значение :attribute невалидное.',
    'image'                   => 'Должно быть выбрано изображение (jpeg, png, gif, bmp).',
    'in'                      => 'Выбранное значение :attribute невалидное.',
    'integer'                 => ':attribute должно быть числом.',
    'ip'                      => ':attribute должен быть валидным IP адресом.',
    'max'                     => [
        'numeric' => ':attribute не может быть больше чем :max.',
        'file'    => ':attribute не может превышать :max Кб.',
        'string'  => ':attribute не может быть больше :max символов.',
        'array'   => ':attribute не может иметь больше :max элементов.',
    ],
    'mimes'                   => ':attribute должен быть файлом в формате: :values.',
    'min'                     => [
        'numeric' => ':attribute должно быть больше :min.',
        'file'    => ':attribute должно быть больше :min Кб.',
        'string'  => ':attribute должно быть больше :min символа.',
        'array'   => 'Должно быть выбрано не менее :min элементов.',
    ],
    'not_in'                  => 'Выбранное значение :attribute невалидное.',
    'not_php'                 => 'Неверный тип файла.',
    'numeric'                 => ':attribute должно быть числом.',
    'regex'                   => 'Неверный формат поля.',
    'required'                => 'Необходимо заполнить это поле.',
    'required_only_on_create' => 'Необходимо заполнить это поле.',
    // 'required_if'             => 'The :attribute field is required when :other is :value.',
    'required_if'             => 'Необходимо заполнить это поле.',
    // 'required_with'           => 'The :attribute field is required when :values is present.',
    'required_with'           => 'Необходимо заполнить это поле.',
    // 'required_with_all'       => 'The :attribute field is required when :values is present.',
    'required_with_all'       => 'Необходимо заполнить это поле.',
    // 'required_without'        => 'The :attribute field is required when :values is not present.',
    'required_without'        => 'Необходимо заполнить это поле.',
    // 'required_without_all'    => 'The :attribute field is required when none of :values are present.',
    'required_without_all'    => 'Необходимо заполнить это поле.',
    'same'                    => ':attribute и :other должны совпадать.',
    'size'                    => [
        'numeric' => ':attribute должно быть :size.',
        'file'    => ':attribute должно быть :size Кб.',
        'string'  => ':attribute должно быть :size символов.',
        'array'   => ':attribute должно содержать :size элементы.',
    ],
    'unique'                  => 'Это поле должно быть уникальным. Подобная запись уже существует.',
    'url'                     => ':attribute невалидный URL.',
    'url_stub'                => 'Неверный формат поля.',
    'url_stub_full'           => 'Неверный формат поля.',
    'not_image'               => 'Файл не является изображением',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
