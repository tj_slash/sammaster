<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
	/**
     * The settings to add.
     */
    protected $settings = [
        [
            'key'         => 'contact_phone',
            'name'        => 'Контактный номер телефона',
            'description' => 'Контактный номер телефона',
            'value'       => '+7 924 841 33 13',
            'field'       => '{"name":"value","label":"Value","type":"text"}',
            'active'      => 1,
        ],
        [
            'key'         => 'contact_email',
            'name'        => 'Контактный email',
            'description' => 'Контактный email',
            'value'       => 'sammaster.pro@mail.ru',
            'field'       => '{"name":"value","label":"Value","type":"email"}',
            'active'      => 1,
        ],
        [
            'key'         => 'contact_address',
            'name'        => 'Адрес организации',
            'description' => 'Адрес организации',
            'value'       => '675000 Благовещенск, ул. Текстильная 48',
            'field'       => '{"name":"value","label":"Value","type":"text"}',
            'active'      => 1,
        ],
        [
            'key'         => 'contact_facebook',
            'name'        => 'Ссылка на организацию в Facebook',
            'description' => 'Ссылка на организацию в Facebook',
            'value'       => 'https://www.facebook.com/',
            'field'       => '{"name":"value","label":"Value","type":"url"}',
            'active'      => 1,
        ],
        [
            'key'         => 'contact_instagram',
            'name'        => 'Ссылка на организацию в Instagram',
            'description' => 'Ссылка на организацию в Instagram',
            'value'       => 'https://www.instagram.com',
            'field'       => '{"name":"value","label":"Value","type":"url"}',
            'active'      => 1,
        ],
        [
            'key'         => 'footer_info',
            'name'        => 'Информация в подвале сайта',
            'description' => 'Информация в подвале сайта',
            'value'       => 'ООО «Напильник», 123456, г.Москва, ул.Центральная 1, офис 1; ИНН 1234567890 ОГРН 1234567890',
            'field'       => '{"name":"value","label":"Value","type":"textarea"}',
            'active'      => 1,
        ],
        [
            'key'         => 'meta_title',
            'name'        => 'Meta title',
            'description' => 'Meta title',
            'value'       => 'СамМАСТЕР - наш инструмент вам в помощь',
            'field'       => '{"name":"value","label":"Value","type":"text"}',
            'active'      => 1,
        ],
        [
            'key'         => 'meta_keywords',
            'name'        => 'Meta keywords',
            'description' => 'Meta keywords',
            'value'       => 'Инструмент',
            'field'       => '{"name":"value","label":"Value","type":"text"}',
            'active'      => 1,
        ],
        [
            'key'         => 'meta_description',
            'name'        => 'Meta description',
            'description' => 'Meta description',
            'value'       => 'Инструмент',
            'field'       => '{"name":"value","label":"Value","type":"textarea"}',
            'active'      => 1,
        ],
        [
            'key'         => 'request_email',
            'name'        => 'E-mail для заявок',
            'description' => 'E-mail для заявок',
            'value'       => 'sammaster.pro@mail.ru',
            'field'       => '{"name":"value","label":"Value","type":"email"}',
            'active'      => 1,
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('settings')->truncate();
        foreach ($this->settings as $index => $setting) {
            $result = DB::table('settings')->insert($setting);
        }
    }
}
