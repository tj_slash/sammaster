<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Page;
use App\Models\Product;

class IndexController extends Controller
{
    public function index()
    {
    	$products = Product::orderBy('lft', 'ASC')->get();

    	$page_delivery = Page::where('slug', 'usloviya-oplaty-i-dostavki')->first();

    	return view('index/index', compact('page_delivery', 'products'));
    }
}
