<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Http\Requests\PageRequest as StoreRequest;
use App\Http\Requests\PageRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class PageCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class PageCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Page');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/page');
        $this->crud->setEntityNameStrings('страницу', 'страницы');


        $this->crud->addColumns([
            [
                'name' => 'title',
                'label' => 'Заголовок страницы'
            ],
            [
                'name' => 'slug',
                'label' => 'Алиас'
            ]
        ]);

        $this->crud->addFields([
            [
                'name' => 'title',
                'label' => 'Заголовок страницы'
            ],
            [
                'name' => 'content',
                'label' => 'Содержимое страницы',
                'type' => 'wysiwyg'
            ],
            [
                'name' => 'meta_title',
                'label' => 'Meta title',
                'type' => 'text'
            ],
            [
                'name' => 'meta_keywords',
                'label' => 'Meta keywords',
                'type' => 'text'
            ],
            [
                'name' => 'meta_description',
                'label' => 'Meta description',
                'type' => 'text'
            ]
        ]);

        $this->crud->denyAccess(['delete']);

        // add asterisk for fields that are required in PageRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
