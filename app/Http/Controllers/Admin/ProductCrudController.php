<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ProductRequest as StoreRequest;
use App\Http\Requests\ProductRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class ProductCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ProductCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Product');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/product');
        $this->crud->setEntityNameStrings('товар', 'товары');

        $this->crud->addColumns([
            [
                'name' => 'title',
                'label' => 'Название'
            ],
            [
                'name' => 'slug',
                'label' => 'Алиас'
            ],
            [
                'name' => 'price',
                'label' => 'Цена'
            ],
            [
                'name' => 'old_price',
                'label' => 'Старая цена'
            ]
        ]);

        $this->crud->addFields([
            [
                'name' => 'title',
                'label' => 'Название'
            ],
            [
                'name' => 'images',
                'label' => 'Изображение',
                'type' => 'browse_multiple',
                'multiple' => true
            ],
            [
                'name' => 'content',
                'label' => 'Описание',
                'type' => 'wysiwyg'
            ],
            [
                'name' => 'price',
                'label' => 'Цена',
                'type' => 'number'
            ],
            [
                'name' => 'old_price',
                'label' => 'Старая цена',
                'type' => 'number'
            ],
            [
                'name' => 'meta_title',
                'label' => 'Meta title',
                'type' => 'text'
            ],
            [
                'name' => 'meta_keywords',
                'label' => 'Meta keywords',
                'type' => 'text'
            ],
            [
                'name' => 'meta_description',
                'label' => 'Meta description',
                'type' => 'text'
            ]
        ]);

        // add asterisk for fields that are required in ProductRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');

        $this->crud->enableReorder('title', 0);
        $this->crud->allowAccess('reorder');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
