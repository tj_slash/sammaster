<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Page;

class PageController extends Controller
{
	/**
	 * View static page
	 *
     * @param string $slug
	 *
	 * @return \Illuminate\Http\Response
	 */
    public function page($slug)
    {
        $page = Page::where('slug', $slug)->firstOrFail();
        return view('pages/page', compact('page'));
    }

    /**
     * Preview static page
     *
     * @param string $slug
	 *
	 * @return \Illuminate\Http\Response
     */
    public static function preview($slug)
    {
    	$page = Page::where('slug', $slug)->first();
        return view('pages/preview', compact('page'));
    }
}
