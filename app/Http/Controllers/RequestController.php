<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RequestRequest as StoreRequest;
use App\Mail\RequestMail;
use Illuminate\Support\Facades\Mail;

class RequestController extends Controller
{
    public function add(StoreRequest $request)
    {
    	$request = \App\Models\Request::create($request->all());
    	Mail::to(\Config::get('settings.request_email'))->send(new RequestMail($request));
    	return response()->json([]);
    }
}
