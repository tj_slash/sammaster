<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Request extends Model
{
    use CrudTrait;

    protected $table = 'requests';

    protected $guarded = ['id'];

    protected $fillable = [];

    /**
     * Get product
     * @return [type] [description]
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
