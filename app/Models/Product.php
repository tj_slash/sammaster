<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Cviebrock\EloquentSluggable\Sluggable;

class Product extends Model
{
    use CrudTrait;
    use Sluggable;

    protected $table = 'products';

    protected $guarded = ['id'];

    protected $fillable = [];

    protected $casts = [
        'images' => 'json'
    ];

    /**
     * Sluggable configuration
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    /**
     * Get requests
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function requests()
    {
        return $this->hasMany(Request::class);
    }
}
