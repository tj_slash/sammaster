<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RequestMail extends Mailable
{
    use Queueable, SerializesModels;

    public $requestUser;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($requestUser)
    {
        $this->requestUser = $requestUser;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Новая заявка на сайте!')
            ->view('emails.request');
    }
}
