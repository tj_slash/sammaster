<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'IndexController@index')->name('index');
Route::group(['prefix' => '/page'], function () {
    Route::get('{slug}', 'PageController@page')->name('page');
});
Route::group(['prefix' => '/request'], function () {
    Route::post('add', 'RequestController@add')->name('request.add');
});
Auth::routes();